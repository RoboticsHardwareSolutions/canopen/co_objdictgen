#!/bin/bash
open -a Xquartz
pkill -9 socat
process_id=$!
wait $process_id
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
