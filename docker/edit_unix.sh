#!/bin/bash

SCRIPT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]}"))

xhost +local:root 1>/dev/null 2>&1

display="${DISPLAY:-:0}"

docker run --name objdictedit_editing --rm --user=$(id -u):$(id -g) --env DISPLAY=${display} -v /tmp/.X11-unix:/tmp/.X11-unix -v $SCRIPT_DIR/workdir:/workdir:rw -it docpillman/co_objdictedit:inital

xhost -local:root 1>/dev/null 2>&1
