#!/bin/zsh

./socat.sh &

SCRIPT_DIR=$(cd "$(dirname -- "$1")" >/dev/null; pwd -P)/$(basename -- "$1")

IP=$(ifconfig -l | xargs -n1 ipconfig getifaddr)

docker run  -v $SCRIPT_DIR/workdir:/workdir:rw  -e DISPLAY=$IP:0 -it docpillman/co_objdictedit:inital
