# co_objdictgen

1. The co_objdictgen contains the Gnosis_Utils-1.2.2.tar.gz archive.  
   > If you want to download:
   > 
   >> For Linux ```$ wget http://gnosis.cx/download/Gnosis_Utils.More/Gnosis_Utils-1.2.2.tar.gz```
   >>
   >> For Windows ```http://gnosis.cx/download/Gnosis_Utils.More/Gnosis_Utils-1.2.2.tar.gz```

2. Install Gnosis
   > For Linux
   > ```
   > $ sudo tar xzf Gnosis_Utils-1.2.2.tar.gz
   > $ cd Gnosis_Utils-1.2.2/
   > $ sudo python setup.py install_all
   > ```
   > For Windows
   > 
   > Unpack Gnosis_Utils-1.2.2.tar.gz
   > ```
   > > cd Gnosis_Utils-1.2.2/
   > > python setup.py install_all
   > ```
4. Install wxPython 2.8 for your Python version from https://sourceforge.net/projects/wxpython/files/wxPython/2.8.11.0/
   > Example:
   > 
   > For Windows x64 and Python 2.7 - **wxPython2.8-win64-unicode-2.8.11.0-py27.exe**
5. There are two implementations in the project - for the terminal (objdictgen.py) and for the GUI (objdictedit.py).
   > Launch for the terminal:
   > 
   >> Linux ```$ sudo python2 objdictgen.py examples/example_objdict.od examples/example_objdict.c```
   >>
   >> Windows ```> python2 objdictgen.py examples/example_objdict.od examples/example_objdict.c```
   >    
   > Launch for the GUI:
   >
   >> Linux ```$ sudo python2 objdictedit.py```
   >> 
   >> Windows ```> python2 objdictedit.py```